<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
?>
<div class="<?php echo ed_school_class( 'container' ); ?>">
	<div class="<?php echo ed_school_class( 'content-fullwidth' ); ?>">
