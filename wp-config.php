<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sekolah_mts');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'tonjoo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^Iu%!]Lq/7]>lw9)Csj|M+Ug:wzeum#LbCY1{L8D0CN$>!y1R+7m9^+ ?mB&[942');
define('SECURE_AUTH_KEY',  '9l1vN&`>J`BZ~K>0=d0H:zh |gUSj.-nlW1jX*8M-OFjdYmsZqZv$mFFT&&y|;7o');
define('LOGGED_IN_KEY',    '_q(gJptP}(Q9~yTn&FDaGGJGJ+I LO7U+mzb8caY&[]NJCY;%%.%34^mtz[[]ELH');
define('NONCE_KEY',        'zY$ZZ,)U(^eioNQ86F, {}w*1:;);pn9tN(_53YK=T o@O~([?Q*!V,[)Ry3skg+');
define('AUTH_SALT',        'BHn#udfMps|e1u*Qq=0_+rf-gATDItWl]%nSRn]$j<9Kzz$%SO@1tMys2#i,L8&i');
define('SECURE_AUTH_SALT', 'YAkgL:) .kVUnbXa:M=@(3xnZ`,NqKh.PA^ZuP3FSJa?ZzR5TR$17::_[rd6|Cy3');
define('LOGGED_IN_SALT',   'r=3C!%knBqZd<_C3a#oqfUW]c_)3MDJ9OtP;#r#L3$vXr-vzBG96e7g%.#hE.6gu');
define('NONCE_SALT',       '1tIi,THCQgB2aw/43LP@N/8s#h^)r%kS?KG:#J@3.KGJd^P`.z ABI;Ed$vL6~{b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
